class EquipmentService {

    private val equipments = listOf(
        mapOf("id" to "f2e31afa-f746-49e0-ab69-184c7d1b30de", "name" to "Alarm Central"),
        mapOf("id" to "49deada9-95da-489a-871e-3bbdfe81df9e", "name" to "Magnetic Sensor"),
        mapOf("id" to "53a0adcf-29d2-4701-9eaf-5de276545c7a", "name" to "Photovoltaic Sensor"),
        mapOf("id" to "cd7b3889-4856-4a2f-9b7f-785de436fd2f", "name" to "Siren"),
        mapOf("id" to "3caf56fe-f9fe-4c5b-946e-6e5c92b9edf4", "name" to "Remote Control")
    );
}