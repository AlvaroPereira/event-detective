class CustomerService {

    private val customers = listOf(
        mapOf("id" to "076b2f16-6f7c-4cb1-a59d-0f7079ec3a2f", "name" to "Álvaro Pereira"),
        mapOf("id" to "f6dfa417-1aff-46e0-9066-6cf9e960f2a4", "name" to "Rafael Calmon"),
        mapOf("id" to "03658bf6-fd40-4d9e-891b-45ed2983d96d", "name" to "Guilherme Nogueira"),
        mapOf("id" to "496b6aa3-b952-460b-9a8b-d8afd450498e", "name" to "Lucas Alisson")
    );

    fun getCustomers(): List<Map<String, String>> {
        return customers;
    }
}