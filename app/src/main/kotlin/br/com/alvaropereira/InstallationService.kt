class InstallationService {

    private val installations = listOf(
        mapOf(
            "id" to "e5eb7c82-15e3-4d4c-b862-2c16a664bc37",
            "customerId" to "03658bf6-fd40-4d9e-891b-45ed2983d96d",
            "address" to mapOf(
                "street" to "Rua Bias Peixoto",
                "latitude" to -27.60729231640282,
                "longitude" to -48.590317492556586
            ),
            "items" to listOf(
                mapOf(
                    "id" to "dd639ace-79ea-4ef9-a739-d6730c6c931b",
                    "equipmentId" to "f2e31afa-f746-49e0-ab69-184c7d1b30de"
                ),
                mapOf(
                    "id" to "b3e3fbe0-2524-46d6-a3fe-6ddd54216f4b",
                    "equipmentId" to "53a0adcf-29d2-4701-9eaf-5de276545c7a"
                ),
                mapOf(
                    "id" to "6211845b-08e7-4f08-a9fa-f94e0add1835",
                    "equipmentId" to "49deada9-95da-489a-871e-3bbdfe81df9e"
                ),
                mapOf(
                    "id" to "cd7b3889-4856-4a2f-9b7f-785de436fd2f",
                    "equipmentId" to "cd7b3889-4856-4a2f-9b7f-785de436fd2f"
                ),
                mapOf(
                    "id" to "cd7b3889-4856-4a2f-9b7f-785de436fd2f",
                    "equipmentId" to "3caf56fe-f9fe-4c5b-946e-6e5c92b9edf4"
                )
            ),
            "createdAt" to "2023-05-01T03:21:32+0000"
        ),
        mapOf(
            "id" to "d74f8de4-9901-42aa-b7f0-90bfff59c5a0",
            "customerId" to "076b2f16-6f7c-4cb1-a59d-0f7079ec3a2f",
            "address" to mapOf(
                "street" to "Rua das Quaresmeiras",
                "latitude" to -21.247207995671765,
                "longitude" to -44.986040596931424
            ),
            "items" to emptyList<Map<String, String>>(),
            "createdAt" to "2023-05-01T03:21:32+0000"
        )
    )
}